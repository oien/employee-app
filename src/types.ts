export interface IAwsEndpoint<ItemT> {
  Items: ItemT[];
  Count: number;
  ScannedCount: number;
}
