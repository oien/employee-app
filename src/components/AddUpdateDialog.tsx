import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material";
import { IDialogProps } from "./DeleteDialog";
import { Field, Form } from "react-final-form";
import TextField from "./TextField";
import { IEmployee } from "./EmployeeTable";

interface IAddUpdateDialogProps extends Omit<IDialogProps, "handleOnConfirm"> {
  handleOnConfirm(values: Partial<IEmployee>): Promise<void>;
  initialValues?: Partial<IEmployee>;
}

export default function AddUpdateDialog({
  open,
  handleClose,
  handleOnConfirm,
  employeeId,
  employeeName,
  initialValues,
  title,
  ...DialogProps
}: IAddUpdateDialogProps) {
  const onSubmit = async (values: Partial<IEmployee>) => {
    console.log("SUBMITTING", values);
    await handleOnConfirm({ ...values });
    handleClose();
  };

  return (
    <Dialog open={open} onClose={handleClose} {...DialogProps}>
      <DialogTitle>
        {title} {employeeName}
      </DialogTitle>
      <Form
        onSubmit={onSubmit}
        initialValues={initialValues}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <Field<string> name="id" label="ID" component={TextField} placeholder="ID" />
              <Field<string> name="name" label="Name" component={TextField} placeholder="Name" />
              <Field<string> name="salary" label="Salary" component={TextField} placeholder="Salary" />
              <Field<string> name="position" label="Position" component={TextField} placeholder="Position" />
              <Field<string>
                name="workingHours"
                label="Working hours"
                component={TextField}
                placeholder="Working hours"
              />
            </DialogContent>
            <DialogActions>
              <Button color="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button color="primary" variant="contained" type="submit">
                Confirm
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  );
}
