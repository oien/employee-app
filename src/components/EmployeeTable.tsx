import { DataGrid, GridActionsCellItem, GridActionsColDef, GridColDef, GridRowParams } from "@mui/x-data-grid";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { useState, useEffect, useMemo, useCallback } from "react";
import { Container, Button } from "@mui/material";
import DeleteDialog from "./DeleteDialog";
import AddUpdateDialog from "./AddUpdateDialog";
import { API_URL } from "../constants";
import type { IAwsEndpoint } from "../types";

const defaultColumn: Partial<GridColDef> = {
  align: "center",
  headerAlign: "center",
  sortable: false,
  disableColumnMenu: true,
  flex: 1,
};

export interface IEmployee {
  id: string;
  name: string;
  workingHours: string;
  salary: number;
  position: string;
}

export default function EmployeeTable() {
  const [rows, setRows] = useState<IEmployee[]>([]);
  const [deleteOpen, setDeleteOpen] = useState(false);
  const [addUpdateOpen, setAddUpdateOpen] = useState(false);
  const [currentRow, setCurrentRow] = useState<Partial<IEmployee>>();

  const handleOpen = (id: "delete" | "update") => (id === "delete" ? setDeleteOpen(true) : setAddUpdateOpen(true));
  const handleClose = (id: "delete" | "update") => (id === "delete" ? setDeleteOpen(false) : setAddUpdateOpen(false));

  // Has more keys than IEmployee, but not interesting
  const handleOpenDialog = useCallback(
    (dialog: "delete" | "update", row: Partial<IEmployee>) => () => {
      setCurrentRow(row);
      handleOpen(dialog);
    },
    [handleOpen]
  );

  const specificColumns: Array<GridActionsColDef | GridColDef> = useMemo(
    () => [
      {
        field: "id",
        headerName: "ID",
      },
      {
        field: "name",
        headerName: "Name",
      },
      {
        field: "workingHours",
        headerName: "Working hours",
      },
      {
        field: "salary",
        headerName: "Salary",
        type: "number",
      },
      {
        field: "position",
        headerName: "Position",
      },
      {
        field: "actions",
        type: "actions",
        getActions: (params: GridRowParams) => [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            showInMenu
            onClick={handleOpenDialog("update", params.row as IEmployee)}
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            showInMenu
            onClick={handleOpenDialog("delete", params.row as IEmployee)}
          />,
        ],
      },
    ],
    [handleOpenDialog]
  );

  const columns = useMemo(
    () =>
      Array.from(
        { length: 6 },
        (_, i) =>
          ({
            ...defaultColumn,
            ...specificColumns[i],
          } as GridColDef | GridActionsColDef)
      ),
    [specificColumns]
  );

  const handleOnDelete = useCallback(
    (id?: string) => async () => {
      const response = await fetch(`${API_URL}/${id}`, { mode: "cors", method: "DELETE" });
      console.log(await response.text());
      handleClose("delete");
      updateRows();
    },
    [handleClose]
  );

  const handleOnAddUpdate = async (employee: Partial<IEmployee>) => {
    const response = await fetch(`${API_URL}`, {
      mode: "cors",
      method: "PUT",
      body: JSON.stringify(employee),
    });
    console.log(await response.text());
    handleClose("update");
    updateRows();
  };

  const getRows = async () => {
    let data = [] as IEmployee[];
    try {
      const response = await fetch(API_URL, { mode: "cors" });
      const json: IAwsEndpoint<IEmployee> = await response.json();
      data = [...json.Items];
    } catch (e) {
      console.log(e);
    } finally {
      return data;
    }
  };

  const updateRows = async () => {
    const rows = await getRows();
    setRows(rows);
  };

  useEffect(() => {
    updateRows();
  }, []);

  return (
    <Container>
      <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
        <div style={{ flexGrow: 1, minHeight: "200px" }}>
          <DataGrid disableColumnMenu hideFooter autoHeight rows={rows} columns={columns} />
        </div>
      </div>
      <DeleteDialog
        handleClose={() => handleClose("delete")}
        handleOnConfirm={handleOnDelete(currentRow?.id)}
        employeeId={currentRow?.id}
        employeeName={currentRow?.name}
        open={deleteOpen}
      />
      <AddUpdateDialog
        initialValues={currentRow}
        title="Add or update"
        handleClose={() => handleClose("update")}
        handleOnConfirm={handleOnAddUpdate}
        employeeId={currentRow?.id}
        employeeName={currentRow?.name}
        open={addUpdateOpen}
      />
      <Button
        variant="contained"
        fullWidth
        style={{ margin: "16px 0px 16px" }}
        onClick={handleOpenDialog("update", {})}
      >
        Add new employee
      </Button>
    </Container>
  );
}
