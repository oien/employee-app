import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import type { DialogProps } from "@mui/material/Dialog";
import DeleteIcon from "@mui/icons-material/Delete";

export interface IDialogProps extends DialogProps {
  handleOnConfirm(): void;
  handleClose(): void;
  employeeName?: string;
  employeeId?: string;
}

export default function DeleteDialog({ open, handleClose, handleOnConfirm, employeeId, employeeName }: IDialogProps) {
  const handleDeleteClose = () => {
    handleOnConfirm();
    handleClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Delete {employeeId} {employeeName && `- ${employeeName}`}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to delete {employeeName} ({employeeId})?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={handleClose}>
          Cancel
        </Button>
        <Button variant="contained" color="primary" startIcon={<DeleteIcon />} onClick={handleDeleteClose} autoFocus>
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
}
