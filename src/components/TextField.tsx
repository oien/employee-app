import { default as MuiTextField } from "@mui/material/TextField";
import { FieldRenderProps } from "react-final-form";

type TTextFieldProps = FieldRenderProps<string, any>;

// interface ITextField extends StandardTextFieldProps, FieldProps<string, HTMLInputElement, string> {}

export default function TextField({ input, meta, ...rest }: TTextFieldProps) {
  return <MuiTextField fullWidth margin="normal" {...input} {...rest} />;
}
